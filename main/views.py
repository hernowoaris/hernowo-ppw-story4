from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def profile(request):
    return render(request, 'main/profile.html')

def sekolah(request):
    return render(request, 'main/sekolah.html')

def games(request):
    return render(request, 'main/games.html')

def story1(request):
    return render(request, "main/story1.html")

def watchlist(request):
    return render(request, "main/watchlist.html")
