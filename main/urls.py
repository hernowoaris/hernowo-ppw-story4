from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('sekolah/', views.sekolah, name='sekolah'),
    path('games/', views.games, name='games'),
    path('story1/', views.story1, name="story1"),
    path('watchlist', views.watchlist, name='watchlist')
]
